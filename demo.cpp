#include <iostream>
#include <vector>
#include <list>
#include <map>
#include "fmt_util.h"

using namespace pg::util::stringUtil;

int main () {

    // 数字
    std::cout << format("1 + 4 = {0}, {0} = 1 + 4, {2} + {1} = {0}, {3}\n", 1+4, 1, 4, 3.14);

    // 字符串
    std::cout << format("Hello {0}\n", "Wolrd");

    // stl-容器
    std::vector<std::vector<int>> v = {{1, 2}, {3, 4}, {5, 6}};
    std::list<std::vector<double>> l = {{1.2, 3.14}, {6.25, 3.7, 4.0}};
    std::map<int, std::vector<std::string>> m = {
        {0, {"0", "零"}},
        {1, {"1", "一", "壹"}}
    };

    std::cout << format("v : {0},\nl : {2},\nm : {1}\n", v, l, m);

    return 0;
}